module.exports = {
  reasonCodes: {
    NOT_SUCCESSFULL_STATUS_CODE_FROM_BINANCE: 'HuobiNotAccepted',
    EXCEPTION_TRYING_CONNECTING_TO_BINANCE: 'HuobiNotReachable',
    RECEIVED_INVALID_MESSAGE: 'ReceivedInvalidMessage',
    DISCONNECTED_FROM_SERVICE: 'DisconnectedFromService',
    STREAM_COMPLETED: 'HuobiStreamStopped',
    STREAM_STARTED: 'HuobiStreamStarted'
  },
  communicationTopology: {
    TOPIC_MARKET_ORDER: 'SagaTaskOrder',
    ROUTING_KEY_UPDATE: 'update',
    ROUTING_KEY_STOP_HUOBI: 'huobi.stop',
    ROUTING_KEY_START_HUOBI: 'huobi.start',
    QUEUE_HUOBI_HANDLER: 'MarketOrder_HuobiHandler'
  }
}
