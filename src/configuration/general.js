module.exports = {
  proxyConnStr: process.env.HTTPS_PROXY || null,
  devLogger: process.env.DEV_LOGGER || true,
  huobiWSurl: process.env.HUOBI_WS_API || 'wss://api.huobi.pro/ws',
  wssPort: process.env.PORT || '8889',
  wssHost: process.env.WSS_HOST || 'localhost'
}
