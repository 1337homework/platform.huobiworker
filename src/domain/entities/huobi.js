const WebSocket = require('ws')
const websockets = require('../../interfaces/websockets')
const generalConfig = require('../../configuration/general')
const { DISCONNECTED_FROM_SERVICE } = require('../../configuration/constants')
const { logger } = require('../../utils/logger')
const { handleError } = require('../../utils/asyncHandlers')
const pako = require('pako')

let client = null

const disconnect = () => {
  if (client) {
    client.close()
  }
}

const connect = () => {
  disconnect()
  const ws = websockets.getServer()
  let url = generalConfig.huobiWSurl

  if (generalConfig.proxyConnStr) {
    let HttpsProxyAgent = require('https-proxy-agent')
    let agent = new HttpsProxyAgent(generalConfig.proxyConnStr)
    client = new WebSocket(url, { agent })
  } else {
    client = new WebSocket(url)
  }

  client.on('open', () => {
    let symbols = ['xrpusdt', 'btcusdt', 'ethusdt']

    for (let symbol of symbols) {
      client.send(JSON.stringify({
        sub: `market.${symbol}.kline.1min`,
        id: ws.clientId
      }))
    }

    logger.log({ level: 'info', message: `Worker connected to Huobi WS API Service...` })
  })

  client.on('error', (err) => {
    logger.log({ level: 'error', message: `Huobi WS Service error ${err}` })
    handleError(
      DISCONNECTED_FROM_SERVICE,
      `Error in Huobi Websocket Service clientId: ${ws.clientId}`,
      {},
      err
    )
  })

  client.on('close', (err) => {
    logger.log({ level: 'info', message: `Huobi WS API Service closed with ${err}` })
  })

  client.on('message', (msg) => {
    let text = pako.inflate(msg, {
      to: 'string'
    })
    let messageObj = JSON.parse(text)

    if (messageObj.ping) {
      client.send(JSON.stringify({
        pong: messageObj.ping
      }))
    } else if (messageObj.tick) {
      messageObj.marketName = 'huobi'
      ws.broadcast(messageObj)
    } else {
      logger.log({ level: 'info', message: `Message was ${text}` })
    }
  })
}

module.exports = {
  connect,
  disconnect
}
