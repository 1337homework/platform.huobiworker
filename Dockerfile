FROM node:alpine

LABEL authors="Valdas Mazrimas <valdas.mazrimas@gmail.com>"
WORKDIR /srv/huobiworker

COPY package*.json ./
RUN npm install --only=production

COPY ./src ./src

EXPOSE 8888

CMD [ "npm", "start" ]